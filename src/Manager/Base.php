<?php

/**
 * Created by PhpStorm.
 * User: Didac
 * Date: 31/07/2016
 * Time: 19:10
 */

namespace Manager;

use pocketmine\block\Block;
use pocketmine\event\block\BlockBreakEvent;
use pocketmine\event\Listener;
use pocketmine\item\Item;
use pocketmine\plugin\PluginBase;

class Base extends PluginBase implements Listener{

    /**
     * Enable the plugin
     */
    public function onEnable(){
        $this->getLogger()->info("CutClean was enabled.");
        $this->getServer()->getPluginManager()->registerEvents($this, $this);
    }

    public function onDisable(){
        $this->getLogger()->info("CutClean was disabled.");
    }

    /**
     * @param BlockBreakEvent $event
     */
    public function onBreak(BlockBreakEvent $event){
        switch($event->getBlock()->getId()){
            case Block::COAL_ORE:
                $event->setDrops([Item::get(Item::TORCH)]);
            break;
            case Block::IRON_ORE:
                $event->setDrops([Item::get(Item::IRON_INGOT)]);
            break;
            case Block::GOLD_ORE:
                $event->setDrops([Item::get(Item::GOLD_INGOT)]);
            break;
            case Block::GRAVEL:
                $event->setDrops([Item::get(Item::FLINT)]);
            break;
        }
    }
}